﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//https://www.codingame.com/training/easy/temperatures

namespace Temperatures
{
    class Program
    {
        static void Main()
        {
            int @default = 0;
            int posResult = 5526;
            int negResult = -273;
            bool negNumExists = false;


            Console.WriteLine("Enter the number of temperatures to analyze = ");


            int n = int.Parse(Console.ReadLine()); // the number of temperatures to analyse
            int[] temps = new int[n];

            Console.WriteLine("Enter the temperatures");

            string[] inputs = Console.ReadLine().Split(' ');
            for (int i = 0; i < inputs.Length; i++)
            {
                int t = int.Parse(inputs[i]); // a temperature expressed as an integer ranging from -273 to 5526
                temps[i] = t;
            }

            if (n == 0)
            {
                Console.WriteLine("There is no data to analyize, default return = " + @default);
            }

            else
            {
                for (int i = 0; i < temps.Length; i++)
                {
                    if (temps[i] <= posResult && temps[i] >= 0)
                    {
                        posResult = temps[i];
                    }

                    else
                    {
                        negResult = temps[i];
                        negNumExists = true;
                    }
                }

                int testingAbs;
                testingAbs = Math.Abs(negResult);

                if (negNumExists == true && testingAbs < posResult)
                {
                    Console.WriteLine(negResult);
                }
                else
                {
                    Console.WriteLine(posResult);
                }
            }


            Main();
        }
    }
}
